package com.hcl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int orderid;
	
	@Column(name="movieid")
	private int movieid;
	@Column(name="snacksid")
	private int snacksid;
	@Column(name="userid")
	private int userid;
	
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Order(int orderid, int movieid, int snacksid, int userid) {
		super();
		this.orderid = orderid;
		this.movieid = movieid;
		this.snacksid = snacksid;
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "Order [orderid=" + orderid + ", movieid=" + movieid + ", snacksid=" + snacksid + ", userid=" + userid
				+ "]";
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public int getMovieid() {
		return movieid;
	}

	public void setMovieid(int movieid) {
		this.movieid = movieid;
	}

	public int getSnacksid() {
		return snacksid;
	}

	public void setSnacksid(int snacksid) {
		this.snacksid = snacksid;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}
	
	

}
